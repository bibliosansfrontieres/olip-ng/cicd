# Customization

The intensive use of `include:` and `variables:`
enables for full customization by various means:

* Jobs Definitions can be extended - or even superseded.
* Variables can be superseded, thanks to the [Variables precedence rules](https://docs.gitlab.com/ee/ci/variables/index.html#cicd-variable-precedence).
* Project specific jobs can be added

## Override Variables

The `deploy.pipeline.yml` file uses the `.deploy-definition` job,
which is written as such:

```yamlm
  script:
    - |
      helm upgrade \
        --wait \
        [...]
        --set image.port="$IMAGE_PORT" \
        [...]
        ./charts
```

And the `$IMAGE_PORT` is defined in the `deploy.definitions.yml` file:

```yaml
  variables:
    IMAGE_PORT: 80  # most of the apps will want to override this in their own/local CI file
```

Since your App uses port 3000, we can overwrite the `$IMAGE_PORT` variable:

```yaml
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.1

variables:
    IMAGE_PORT: 3000
```

Note that this variable has been (re)defined at the top level in the YAML file.
This override will be inherited by any other job using the same variable name
(finding out why this is bad practice is left as an exercise for the reader).

As a rule of thumb you should always restrict overrides to the related job.
This can be achieved by extending the `&deploy-definition` within the `deploy:` job:

```yaml
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.1

deploy:
  extends:
    - .deploy-definition
  variables:
    IMAGE_PORT: 3000
```

In this case the (new) variable value is only effective in the `deploy:` job.

## Extend included Definitions

You can rewrite a Job Definition, from the simple statement replacement
to a full job rewrite.

For example, the `shellcheck` linter uses the `stable` tag from `koalaman/shellcheck-alpine`.
You can use a newer version by overriding the `image:` key:

```yaml
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.1

shellcheck:
  extends:
    - .lint-shellcheck
  image: koalaman/shellcheck-alpine:latest
```

Or you could make a lot of friends by making sure tests always pass,
by completely rewriting the job:

```yaml
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.1

shellcheck:
  stage: lint
  image: busybox
  script:
    - echo "This is fine."
```

## Add specific jobs

You can add project specific jobs in your `.gitlab-ci.yml` file:

```yaml
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.1

call-back-home:
  stage: deploy
  image: curlimages/curl
  script:
    - curl -X POST --data '{"content": "Hello world"}' --header "Content-Type:application/json" https://api.example.com/
```
