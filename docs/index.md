# Introduction

This repository aims to improve the OLIP packaging lifecycle by providing
[Continuous Integration](https://about.gitlab.com/product/continuous-integration/).

Our [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) definition is focused
on speeding up this process by giving developers faster feedback.

## What does this _pipeline_ provide for my project?

The pipeline builds your app(s) and runs multiple checks on them after every push
to GitLab.

This provides you with instant feedback about any problems the changes you made
may have created or solved, without the need to publish the app, speeding up your
development cycle and improving the quality of apps added to the OLIP Catalog.

Having this on Gitlab CI ensures that every app accomplishes the minimum quality
to be in the Catalog and if we improve or add a new service
the project will get the benefit instantaneously.

![OLIP CICD in action](olip-cicd-pipeline.png)

## CICD Stages

The Shared CICD has three stages, and you are free to add your own.

### Lint

A bunch of linters are ran in order to make sure files are valid.

### Build

The Docker Image is built and pushed to Registries.

Docker Hub is the default Registry so we are pushing images there
([`offlineinternet` repository](https://hub.docker.com/u/offlineinternet)).

Images are also pushed to the GitLab Registry.
Images referenced in the OLIP Catalog are pointing to the GitLab Registry
in order to avoid hitting the Docker Hub pull quota.

### Deploy

In order to provide a testing instance where you can check whether your App
works as you want, a testing environment is available for trigger.

It automatically creates an OLIP deployment including your App
in our Kubernetes Cluster. Its URL is available in the left bar in your GitLab project:
`Deployments` > `Environments`.

Note that this `deploy:` job is **manual** ;
however, environments are automatically deleted after one week.
