# Basic Usage

To use the OLIP Pipeline, the first thing to do is to enable the project's Pipeline.
Go to `Settings` (General), expand `Visibility, project features, permissions`,
and in `Repository`, enable `Pipelines`.
This makes the `CI/CD` settings and menu available.

If your app does not require anything specific for building,
then you can just include a Recipe file:

```yaml
---
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.1

stages:
  - lint
  - build
  - deploy
```

Note that you still need to declare the stages ; those are NOT inherited by inclusion.

## What does it do?

It will inherit jobs Definitions, and actual jobs created by Pipelines.

As an example, the `apps.recipe.yml` file includes a complete
`docker.recipe.yml` file, which itself includes `docker-buildx`
Definitions as well as `docker-buildx` Pipeline.

It also includes `linters.definitions` and `linters.pipeline` ;
both files merged together will actually create Linters jobs in
the final CICD pipeline.
