# Contributing

Obviously a Draft.

## References

* Always create a topic branch, in order to use it as a `ref:`.
* Projects `master`/`main` branches should always reference a stable git tag,
  such as `v1.1.1`

## Synopsis

So we want to add a new linter job.

### Test the new job

In the CICD project, create a topic branch: `git co -b linters-shadock`

Add your job, commit, push.

In order to test this pumpy pipeline, in your App project create a new branch:

`git co -b test-shadock-linter`

Change your `include:ref:` from `v1.1.1` to `linters-shadock`.
⚠ note that this reference is a reference within the `cicd` project,
NOT your App project!

Commit, push; thanks to this custom `ref:`,
the CICD topic branch is the one being `include:`d.

#### Warning about nested includes

The Recipe files also include other files and have their own `ref:` key:

```yaml
---
include:
  - project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/docker-buildx.definitions.yml'
      - '/docker-buildx.pipeline.yml'
    ref: v1.1.1
```

When you update the `include:ref` in your App's CI,
it makes it point to the `linters-shadock` branch **for the Recipe file** ;
however, this Recipe file still points at the `v1.1.1` ref,
so in this case the `docker-buildx.pipeline.yml` file is still included
from the `v1.1.0` branch.

This is a common trap, but also an example of how modular this CICD is ;
you want to fine-tune which file you are testing:

```yaml
---
include:
  - project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/docker-buildx.definitions.yml'
    ref: linters-shadock
  - project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/docker-buildx.pipeline.yml'
    ref: v1.1.1
```

### Promote to production

You tested everything work and want to push it to Production.

* Update the nested `include:ref:` keys
* Merge your topic branch to `main`
* Add a git tag
* Update your App(s) pipeline's `ref:` to use the newly created tag

From then, all projects using the Shared CICD referencing
this new `ref:` will benefit the new job.
