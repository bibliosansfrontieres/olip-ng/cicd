# OLIP Shared CICD

Build and test your OLIP applications on reproducible environments on every push!

## Usage

The most basic usage is to create a `.gitlab-ci.yml` file with this content:

```yaml
---
include:
  - project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/apps.recipe.yml'
    ref: v1.1.0

stages:
  - lint
  - build
  - deploy
```

It will validate your code, build the Docker image,
then finally push it to public Registries.

Much more magic is possible, as described in the documentation.

## Documentation

The full documentation from the [`docs/`](docs/index.md) directory is published at
<https://bibliosansfrontieres.gitlab.io/olip/cicd/>.
