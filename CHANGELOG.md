# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2024-02-01

### Fixed

- Update the refs in nested includes

## [1.1.0] - 2024-02-01

### Added

- Documentation as GitLab Pages (#4)
- Docker images are built with a common set of Labels (b39ed36)

### Fixed

- Added missing tags to jobs (4f8cdc7)

## [1.0.0] - 2023-01-16

- First release
